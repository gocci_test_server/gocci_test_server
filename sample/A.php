<?php
require 'Test.php';

  //testオブジェクトを生成
  $test = new Test();

  //testオブジェクトに値をセット
  $test->setUserId('5');
  $test->setName('5');

  session_start();

  //testオブジェクトをセッションに格納
  $_SESSION['test'] = serialize($test);

?>
