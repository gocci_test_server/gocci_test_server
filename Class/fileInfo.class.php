<?php

class FileInfo 
{
    private $_retCode;//改行コード保存用

    function readFile($file) {
	$this->retCode['CRLF'] = 0;
	$this->retCode['LF']   = 0;

	$fp = fopen($file, 'r');

	while (!feof($fp) {
            if (preg_match('/\r$/', $line) {
	       $this->retCode['CRLF']++;
	    } else {
	        $this->retCode['LF']++;
	    }
        }
        $fclose($fp);
}
//改行コードを返すメソッド
function getRetCode() {
    if ($this->retCode['CRLF'] === 0 ) {
	return 'LF';
    } else if ($this->_retCode['LF'] === 0) {
	return 'CRLF';
    } else {
	return 'CRLF &LF'; // CRLFとLFが混在する場合
    }
}
//new演算子でFileInfoクラスのオブジェクトをインスタンス化する。
$fileInfo = new FileInfo();
echo '改行コードは「' . h($fileInfo->getRetCode()) . '」です。';

function h($strin) {
    return htmlsupecialchars($stirng, ENT_QUOTES);
}
