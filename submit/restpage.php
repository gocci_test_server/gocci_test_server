<?php
/*
restpage.phpは各レストラン毎のタ投稿イムライン
*/
header('Content-Type: text/html; charset=utf-8');
require_once '../Model/function.php';
require_once '../Model/const.php';
//https://codelecture/gocci/restpage.php?restname=へそまんじゅう本舗（へそまんじゅうほんぽ)

try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user, $password);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//print 'DB接続成功';
} catch (PDOException $e) {
    print "error: ". $e->getMessage() . "<br>";
    die();    
}
file_get_contents('php://input');
//restname初期値設定
$restname  = 'restname';  
@$restname = $_POST['restname'];
//$restname = 'へそまんじゅう本舗（へそまんじゅうほんぽ）';
//var_dump($restname);
error_log(var_export($_REQUEST, true));
print $restname;
/*
$query = "SELECT rest.restname,rest.locality,concat(movie_url,movie) AS movies,concat(image_url,picture) AS images,u.user_name,restname
FROM restaurants AS rest
JOIN posts AS p 
ON  p.gocci_rest_id = rest.id
JOIN users AS u
ON u.user_id = p.user_id
WHERE rest.restname = 'へそまんじゅう本舗（へそまんじゅうほんぽ）'
ORDER BY date_time DESC
LIMIT 30
";
*/

$restname = 'restname';

if (isset($_GET['restname'])) { 
    $restname = $_GET['restname'];
}

//$restname = 'へそまんじゅう本舗（へそまんじゅうほんぽ）';
//var_dump($restname);
/*
$query = "SELECT rest.restname,rest.locality,concat(movie_url,movie) AS movie,concat(image_url,p.picture) AS picture,u.user_name,restname
FROM restaurants AS rest
JOIN posts AS p 
ON  p.gocci_rest_id = rest.id
JOIN users AS u
ON u.user_id = p.user_id
WHERE rest.restname = '$restname'
ORDER BY p.date_time DESC
LIMIT 30";
*/

/*
$query = "SELECT rest.restname,rest.locality,u.picture,concat(movie_url,movie) AS movie,u.user_name,restname
FROM restaurants AS rest
JOIN posts AS p 
ON  p.gocci_rest_id = rest.id
JOIN users AS u
ON u.user_id = p.user_id
WHERE rest.restname = '$restname'
ORDER BY p.date_time DESC
LIMIT 30";
/*

/* update 2014/09/29 18:45 the newest*/

$query = "SELECT p.post_id,rest.restname,rest.locality,u.picture,concat(movie_url,movie) AS movie,u.user_name,restname,r.review,p.goodnum,p.comment_num
FROM restaurants AS rest
JOIN posts AS p 
ON  p.gocci_rest_id = rest.id
JOIN users AS u
ON u.user_id = p.user_id
JOIN review as r
ON p.post_id = r.post_id
WHERE rest.restname = '$restname'
ORDER BY p.date_time DESC
LIMIT 30";

//var_dump($query);
$rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
//var_dump($rows);
$json = json_xencode($rows);
header('Context-Type: text/javascript; charset=utf-8');
//print_r($json);
print $json;
?>

