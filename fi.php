<?php
// データベース設定
$host = "localhost"; // ホスト
$user = "root"; // ユーザー名
$password = "iwtbls_code"; // パスワード
$dbname = "gocci"; // データベース名

// データベースにアクセス
// 文字コードutf8、MySQL
$pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user, $password);
//mysqli_set_charset($pdo, 'UTF8');
// jsonファイルの読み込み
$file = file_get_contents("./*.json", false);
// jsonファイルのデコード
$restaurant_list = json_decode($file);

// 挿入クエリ
$query = "INSERT INTO restaurants (id, category, homepage, lat, lon, restname, tell, locality) VALUES (NULL, :category, :homepage, :lat, :lon, :restname, :tell, :locality);";
// クエリの事前登録
$statement = $pdo->prepare($query);
// jsonファイルをループしてデータベースに挿入	
foreach ($restaurant_list as $station_name => $restrant) {
	foreach ($restrant as $key => $value) {
		$statement->execute(array(
				':category' => implode(",", $value->Category),
				':homepage' => $value->Homepage,
				':lat' => $value->Lat,
				':lon' => $value->Lon,
				':restname' => $value->RestName,
				':tell' => $value->Tell,
				':locality' => $value->locality,
			)
		);
		
	}

}

//$statement->fetchAll(PDO::FETCH_ASSOC);


?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
</head>
<body>
</body>
</html>
