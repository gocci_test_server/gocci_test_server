<?php
//user毎の各IDを保持するクラス
class Test {

  var $id;	//userのID
  var $name;	//userのNAME
  var $rest_id; //レストランのID
  var $post_id; //記事の投稿ID

  //各IDを保持
  function setUserid($id) {
    $this->id = $id;
  }
  function setName($name) {
    $this->name = $name;
  }
  function setRestId($rest_id) {
    $this->rest_id = $rest_id;
  }
  function setPostId($post_id) {
    $this->post_id = $post_id;
  }

  //各IDを取得
  function getId() {
    return $this->id;
  }
  function getName() {
    return $this->name;
  }
  function getRestId() {
    return $this->rest_id;
  }
  function getPostId() {
    return $this->post_id;
  }
}
