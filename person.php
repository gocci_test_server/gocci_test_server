<?php
// 人を表すシンプルな実験用のクラス
class Person
{
  // stringで名前
  private $_name;

  // boolで性別
  private $_sex;

  // DateTimeで誕生日
  private $_birthday;

  public function __construct($name, $sex, $birthday)
  {
    $this->_name = $name;
    $this->_sex = $sex;
    $this->_birthday = $birthday;
  }

  public function GetName()
  {
    return $this->_name;
  }

  public function GetSex()
  {
    return $this->_sex;
  }

  public function GetBirthday()
  {
    return $this->_birthday;
  }

  public function ToString()
  {
    return
      "name=". $this->_name .
      " sex=" . ($this->_sex ? "F" : "M") .
      " birthday=" . $this->_birthday->format("Y-m-d");
  }
}
?>
