<?php
header('Content-Type: text/html; charset=utf-8');
//設定ファイル読み込み
require_once './Model/const.php';
require_once './Model/Class.php';

$pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;",$user,$password);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//$post_id = isset($_POST['post_id']) ? $_POST['post_id'] : '';
file_get_contents('php://input');
$post_id = isset($_POST['post_id']) ? $_POST['post_id'] : '';

session_start();
@$test = unserialize($_SESSION['test']);
//var_dump($_SESSION);
//var_dump($test);
//echo $test;
@$user_id =  $test->getId();
$exist_user_id = 0;//初期値
if (!empty($user_id)) {
    $user_id = 1;
    //$user_id = $pdo->lastInsertId();
    $user_id = $test->getId();
    //ログインor登録したuserのuser_idを格納
    $user_id = $_SESSION['user_id'];
    //var_dump($user_id);
}
/* postsテーブルに投稿したuserのuser_idと同じかどうかを判定する。あれば削除処理へ。  */
/*
$user_id =65; 
$exist_user_id = 65; 
$post_id = 1967;
*/
//postsテーブルからuser_idを出す
if (!empty($post_id)) {
    $query = "SELECT user_id FROM posts WHERE post_id = " .$post_id;
    $stmt = $pdo->query($query);
    $result = $stmt->fetch(PDO::FETCH_NUM);
    $exist_user_id = $result[0];
}
//print 'now';
if ((int)$user_id === (int)$exist_user_id) {
//echo 'now2';
    try {
       // if (!empty($post_id)) {
        $pdo->beginTransaction();
        $stmt = $pdo->prepare("UPDATE posts SET permission_id = 0 WHERE post_id = :post_id");
	$stmt->bindParam(":post_id", $post_id);
        $stmt->execute();

        $stmt = $pdo->prepare("UPDATE review SET permission_id = 0 WHERE post_id = :post_id");
	$stmt->bindParam(":post_id", $post_id);
	$stmt->execute();  
	$pdo->commit();
       // }
    } catch (PDOException $e) {
        print 'エラー' . $e->getMessage() . "<br>";
        die();
        $pdo->rollback();
    }  
}
