<?php
require_once './Model/function.php';
require './Model/Class.php';
header('Content-Type: text/html; charset=utf-8');
//$updir = "/gooci/movies";
/*
URLアクセスでこのページにこられたら困るので、header関数等で別のページに飛ばすようにする。
*/
$host = "localhost"; // ホスト
$user = "root"; // ユーザー名
$password = "iwtbls_code"; //パスワード
$dbname = "gocci"; // データベース名

//comment一覧をJSONで出力するAPI
session_start();
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user,$password);
    //SELECT処理
} catch (PDOException $e) {
    print 'エラー' . $e->getMessage() . "<br>";
    die();
}
//$post_id = $_SESSION['post_id'];
/*
$test = unserialize($_SESSION['test']);
echo $test->getPostId();
$post_id = $test->getPostId();
*/

@$post_id = $_GET['post_id'];

//$query = "SELECT post_id FROM posts ORDER BY date_time DESC limit 1 ";
//$maxpost_id = $pdo->query($query)->fetch(PDO::FETCH_NUM);
//print $maxpost_id[0];
/*
$query = "SELECT DISTINCT(c.comment),u.user_name, u.picture,p.date_time
FROM users AS u
JOIN comments AS c
ON u.user_id = c.user_id
JOIN posts AS p
ON u.user_id = p.user_id
WHERE p.post_id = '$post_id'
ORDER BY p.date_time DESC;";
*/

$query = "SELECT DISTINCT(comment),u.user_name,u.picture,c.id
FROM posts AS p
JOIN comments AS c
ON p.post_id = c.post_id
JOIN users AS u
ON c.user_id = u.user_id
WHERE p.post_id = '$post_id'
ORDER BY c.id DESC;";

$rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);

//取得してきた情報をJSON形式で返す
$json = json_xencode($rows);
@header("Content-Type: text/javascript; charset=utf-8");
print $json;
